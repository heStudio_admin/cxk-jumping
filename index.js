let role = document.querySelector('.role');
let tree = document.querySelector('.tree');
let game = document.querySelector('#game');
let tiao = new Audio("./tiao.mp3");
let fail = new Audio("./fail.mp3");
let maoxian = new Audio("./damao.mp3");
let huoli = new Audio("./火力.mp3");
let success = new Audio("./success.mp3");
document.body.addEventListener('touchstart', jump);
let otime = new Date();
function jump(event) {
    if (tiao.play) {
        tiao.pause();
    }
    tiao.play();
    if (event) {
        if (role.classList != "animate") {
            role.classList.add("animate");
        }
        setTimeout(function () {
            role.classList.remove("animate");
        }, 500);
    }
}

var check = setInterval(function () {
    let ntime = new Date();
    let score = ntime - otime;
    if (score >= 3000) {
        maoxian.play();
    } else if (score >= 7000 || score >= 9000) {
        huoli.play();
    }
    if (score >= 12000) {
        tree.style.animation = "none";
        role.style.animation = "none";
        document.querySelector("h2").style.display = "block";
        document.querySelector("button").style.display = "block";
        success.play();
        document.body.removeEventListener('touchstart', jump);
        clearInterval(check);
    }
    document.querySelector("h3").innerText = "成绩：" + score;
    let blockButtom = parseInt(window.getComputedStyle(role)
        .getPropertyValue("bottom"));
    let stopRight = parseInt(window.getComputedStyle(tree)
        .getPropertyValue("right"));
    if (stopRight > 510 && stopRight < 600 && blockButtom < 25) {
        tree.style.animation = "none";
        role.style.animation = "none";
        document.querySelector("p").style.display = "block";
        document.querySelector("button").style.display = "block";
        fail.play();
        document.body.removeEventListener('touchstart', jump);
        clearInterval(check);
    }
}, 10)